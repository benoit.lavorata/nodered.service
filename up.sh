#!/bin/bash
echo ""
echo "Node RED"
echo "Maintainer: Benoit Lavorata"
echo "License: MIT"
echo "==="
echo ""

FILE=".env"
if [ -f $FILE ]; then
    echo ""
    echo "File $FILE exists, will now start: ..."
    export $(cat .env | xargs)

    echo ""
    echo "Create networks ..."
    docker network create $NODERED_CONTAINER_NETWORK

    echo ""
    echo "Create volumes ..."
    docker volume create --name $NODERED_CONTAINER_VOLUME_DATA

    FILE_TEMPLATES=".templates_copied"
    echo ""
    if [ -f $FILE_TEMPLATES ]; then
        echo "Templates already deployed ..."
    else
        echo "Pull image"
        docker pull $NODERED_CONTAINER_IMAGE
        
        echo "Copy template files ..."
        
        #echo ""
        echo "Generate password hash"
        docker run --rm --name ${NODERED_CONTAINER_NAME}_config $NODERED_CONTAINER_IMAGE /usr/local/bin/node -e "console.log(require('bcryptjs').hashSync(process.argv[1], 8));" "$NODERED_ADMIN_PASSWORD" > .hash
        #docker run -it --name ${NODERED_CONTAINER_NAME}_config $NODERED_CONTAINER_IMAGE /bin/bash
        #sleep 1
        HASH=$(cat .hash)
        echo "'"$HASH"'" > .hash
        HASH=$(cat .hash)
        rm $PWD/.hash
        
        echo ""
        echo "Generate settings.js"
        rm $PWD/templates/settings.js
        cp $PWD/templates/settings.template.js $PWD/templates/settings.js
        sed -i "s/NODERED_ADMIN_USER/$NODERED_ADMIN_USER/g" $PWD/templates/settings.js
        sed -i "s#NODERED_ADMIN_PASSWORD#$HASH#g" $PWD/templates/settings.js

        echo "Verify HASH"
        cat $PWD/templates/settings.js | grep password

        echo "generate permissions and files"
        docker compose up -d --remove-orphans $1
        sleep 1
        docker compose down

        SOURCE_BIND=$PWD/templates
        DEST_VOLUME=$NODERED_CONTAINER_VOLUME_DATA
        echo "Copy $SOURCE_BIND/settings.js to $DEST_VOLUME..."
        docker run -u 1001 --rm -v $SOURCE_BIND:/source -v $DEST_VOLUME:/dest -w /source alpine cp /source/settings.js /dest/settings.js
        docker run -u 0 --rm -v $SOURCE_BIND:/source -v $DEST_VOLUME:/dest -w /source alpine chown 1001:1001 /dest/settings.js
        rm $PWD/templates/settings.js
        
        touch $FILE_TEMPLATES
    fi

    echo ""
    echo "Make sure to set admin password"

    echo ""
    echo "Boot ..."
    docker compose up -d --remove-orphans $1
else
    echo "File $FILE does not exist: deploying for first time"
    cp .env.template .env
    
    echo -e "Make sure to modify .env according to your needs, then use ./up.sh again."
fi